/**
 * 
 */

angular.module('myApp').factory('HomeService',function() {
	var validation = {};	

	validation.isValidURL = function (url){
	  return url.match(/^(ht|f)tps?:\/\/[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/);
	   
	};

	validation.isValidNumber = function(number){
		if(isNaN(number)){
			return false;
		}
		return true;
	};

	validation.isValidEmail = function(email){
			var atpos = email.indexOf("@");
		    var dotpos = email.lastIndexOf(".");
		    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
		    
		       return false;
		    }
		    return true;	
		
	};

	validation.isValidUsPhoneNumber = function(phonenumber){
		return phonenumber.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/);
		
	};

	validation.isValidUSZipCode = function(zipcode){
		
		return zipcode.match( /(^\d{5}$)|(^\d{5}-\d{4}$)/);
		
	};

	validation.isValidCreditCard = function(creditcardnumber){
	
		return creditcardnumber.match(/^(?:4[0-9]{12}(?:[0-9]{3})? |5[1-5][0-9]{14}|  3[47][0-9]{13}|  3(?:0[0-5]|[68][0-9])[0-9]{11}|  6(?:011|5[0-9]{2})[0-9]{12} | (?:2131|1800|35\d{3})\d{11})$/)
		
	};
	
	return validation;
	
});