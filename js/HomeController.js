/**
 * 
 */
var app=angular.module('myApp');

app.controller('HomeController', function($scope, $http,HomeService) {

	$scope.resultstobeshown=false;
	$scope.errorMess=[];
	$scope.errorShow=false;
	
	
$scope.validationCheck= function(){

	if(!HomeService.isValidURL($scope.url)){
		$scope.errorMess.push("Not a valid URL");
		$scope.errorShow=true;
	}
	
	if(!HomeService.isValidNumber($scope.number)){
		
		$scope.errorMess.push("Not a valid Number");
		$scope.errorShow=true;
	}
	
	if(!HomeService.isValidEmail($scope.email)){
		$scope.errorMess.push("Not a valid Email");
		$scope.errorShow=true;
	}
	
	if(!HomeService.isValidUsPhoneNumber($scope.phnumber)){
		$scope.errorMess.push("Not a valid Phone number");
		$scope.errorShow=true;
	}
	
	if(!HomeService.isValidUSZipCode($scope.zipcode)){
		$scope.errorMess.push("Not a valid Zip Code");
		$scope.errorShow=true;
	}
	
	if(!HomeService.isValidCreditCard($scope.creditcard)){
		$scope.errorMess.push("Not a valid credit card");
		$scope.errorShow=true;
	}
	
};

var checkEmptyValue = function(value){
	if(value===undefined){
		return true;
	}
}




});